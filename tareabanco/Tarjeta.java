/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tareabanco;

import java.util.Date;

/**
 *
 * @author Esteban
 */
public class Tarjeta {
 private int Numtarjeta;    
 private int Numcuenta;   
 private int Numcliente;
 private Cuenta cuentaTarjeta;
 private Date fechadeEcpiracion;

    public Tarjeta(int Numtarjeta, int Numcuenta, int Numcliente, Date fechadeEcpiracion) {
        this.Numtarjeta = Numtarjeta;
        this.Numcuenta = Numcuenta;
        this.Numcliente = Numcliente;
       
        this.fechadeEcpiracion = fechadeEcpiracion;
    }

    public int getNumtarjeta() {
        return Numtarjeta;
    }

    public void setNumtarjeta(int Numtarjeta) {
        this.Numtarjeta = Numtarjeta;
    }

    public int getNumcuenta() {
        return Numcuenta;
    }

    public void setNumcuenta(int Numcuenta) {
        this.Numcuenta = Numcuenta;
    }

    public int getNumcliente() {
        return Numcliente;
    }

    public void setNumcliente(int Numcliente) {
        this.Numcliente = Numcliente;
    }

    public Cuenta getCuentaTarjeta() {
        return cuentaTarjeta;
    }

    public void setCuentaTarjeta(Cuenta cuentaTarjeta) {
        this.cuentaTarjeta = cuentaTarjeta;
    }

    public Date getFechadeEcpiracion() {
        return fechadeEcpiracion;
    }

    public void setFechadeEcpiracion(Date fechadeEcpiracion) {
        this.fechadeEcpiracion = fechadeEcpiracion;
    }

    @Override
    public String toString() {
        return "Tarjeta{" + "Numtarjeta=" + Numtarjeta + ", Numcuenta=" + Numcuenta + ", Numcliente=" + Numcliente + ", cuentaTarjeta=" + cuentaTarjeta + ", fechadeEcpiracion=" + fechadeEcpiracion + '}';
    }
 
}
