/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tareabanco;

import java.util.ArrayList;

/**
 *
 * @author Esteban
 */
public class Banco {
private int numBanco;    
private String nombre;   
private String direccion;   
private int telefono;
private ArrayList<Cajero>cajeroBanco;
private ArrayList<Cuenta>CuentaBanco;

    public Banco(int numBanco, String nombre, String direccion, int telefono) {
        this.numBanco = numBanco;
        this.nombre = nombre;
        this.direccion = direccion;
        this.telefono = telefono;
       cajeroBanco=new ArrayList();
       CuentaBanco=new ArrayList();
    }
    

public void verificarTransaccion(){}
public void verificarCuenta(){}
public void verificarSaldo(){}

    public int getNumBanco() {
        return numBanco;
    }

    public void setNumBanco(int numBanco) {
        this.numBanco = numBanco;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public int getTelefono() {
        return telefono;
    }

    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }

    public ArrayList<Cajero> getCajeroBanco() {
        return cajeroBanco;
    }

    public void setCajeroBanco(ArrayList<Cajero> cajeroBanco) {
        this.cajeroBanco = cajeroBanco;
    }

    public ArrayList<Cuenta> getCuentaBanco() {
        return CuentaBanco;
    }

    public void setCuentaBanco(ArrayList<Cuenta> CuentaBanco) {
        this.CuentaBanco = CuentaBanco;
    }

    @Override
    public String toString() {
        return "Banco{" + "numBanco=" + numBanco + ", nombre=" + nombre + ", direccion=" + direccion + ", telefono=" + telefono + ", cajeroBanco=" + cajeroBanco + ", CuentaBanco=" + CuentaBanco + '}';
    }
    
}
