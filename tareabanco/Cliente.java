/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tareabanco;

import java.text.ParseException;

import java.util.ArrayList;


/**
 *
 * @author Esteban
 */
public class Cliente {
  private  int NumCliente; 
  private int Identificacion;
  private String Nombre;
  private String Direccion;
  private int Telefono;
  private ArrayList<Cuenta> Cuentacliente;
  private ArrayList<Tarjeta> Tarjetacliente;

    public Cliente(int NumCliente, int Identificacion, String Nombre,
            String Direccion, int Telefono) {
        this.NumCliente = NumCliente;
        this.Identificacion = Identificacion;
        this.Nombre = Nombre;
        this.Direccion = Direccion;
        this.Telefono = Telefono;
        
  Cuentacliente=new ArrayList();
  Tarjetacliente=new ArrayList();
    }
  public void agregarcuenta(Cuenta ca) {

        Cuentacliente.add(ca);
       
  }
  public void agregartarjeta(Tarjeta t1) {

        
        Tarjetacliente.add(t1);
        
     
  }
  public void ingresarClave(){}
  public void elejirClave(){}
  public void indicarValoraretirar(){}

    public int getNumCliente() {
        return NumCliente;
    }

    public void setNumCliente(int NumCliente) {
        this.NumCliente = NumCliente;
    }

    public int getIdentificacion() {
        return Identificacion;
    }

    public void setIdentificacion(int Identificacion) {
        this.Identificacion = Identificacion;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public String getDireccion() {
        return Direccion;
    }

    public void setDireccion(String Direccion) {
        this.Direccion = Direccion;
    }

    public int getTelefono() {
        return Telefono;
    }

    public void setTelefono(int Telefono) {
        this.Telefono = Telefono;
    }

    public ArrayList<Cuenta> getCuentacliente() {
        return Cuentacliente;
    }

    public void setCuentacliente(ArrayList<Cuenta> Cuentacliente) {
        this.Cuentacliente = Cuentacliente;
    }

    public ArrayList<Tarjeta> getTarjetacliente() {
        return Tarjetacliente;
    }

    public void setTarjetacliente(ArrayList<Tarjeta> Tarjetacliente) {
        this.Tarjetacliente = Tarjetacliente;
    }

    @Override
    public String toString() {
        return "Cliente{" + "NumCliente=" + NumCliente + ", Identificacion=" + Identificacion + ", Nombre=" + Nombre + ", Direccion=" + Direccion + ", Telefono=" + Telefono + ", Cuentacliente=" + Cuentacliente + ", Tarjetacliente=" + Tarjetacliente + '}';
    }

   

}
