/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tareabanco;

import java.util.ArrayList;

/**
 *
 * @author Esteban
 */
public class Cajero {
    private String clave ;
    private Banco bancoTransaccion;
    private ArrayList<Transaccion>transaccionCajero=new ArrayList();

    public Cajero(String clave, ArrayList<Transaccion> transaccionCajero) {
        this.clave = clave;
        
        this.transaccionCajero = transaccionCajero;
        
    }
   
    public void mostrarOpciones (){}
    public void solicitarClave(){}
    public void verificarBanco(){}
    public void darRespuesta(){}

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public Banco getBancoTransaccion() {
        return bancoTransaccion;
    }

    public void setBancoTransaccion(Banco bancoTransaccion) {
        this.bancoTransaccion = bancoTransaccion;
    }

    public ArrayList<Transaccion> getTransaccionCajero() {
        return transaccionCajero;
    }

    public void setTransaccionCajero(ArrayList<Transaccion> transaccionCajero) {
        this.transaccionCajero = transaccionCajero;
    }

    @Override
    public String toString() {
        return "Cajero{" + "clave=" + clave + ", bancoTransaccion=" + bancoTransaccion + ", transaccionCajero=" + transaccionCajero + '}';
    }
    
}
